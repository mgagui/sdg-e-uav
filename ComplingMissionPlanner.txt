First Things First - Ensure Debug Configuration is set for x86 
Click >Start button and see if you encounter any errors
Do not update if Misson Planner asks if you want to update

Error List Fixes:

NumpyDotNet - Not Found -> Copy test_dtype.py and test_multiarray.py from Lib/site-packages/numpy/NumpyDotNet/bin/Tests into directory for Mission Planner Source

(Some file not found) -> Mission Planner needs references to downloaded Mission Planner - Debug - Exceptions - Manage Debugging Assistant - Uncheck Loader Lock

LoadLock -> See Mission Planner Building Solution

OSIPI missing libraries -> install pisdkx64 for 64bit OS and setup.exe aftewards

The "SignFile" task was not given a value for the required parameter "CertificateThumbprint"
->Right click MissionPlanner-> Properties-> Signing-> Uncheck Sign the ClickOnce manifests checkbox