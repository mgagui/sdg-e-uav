# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

Script.ChangeMode("LOITER")
print 'Starting descend script'
print 'Current Altitude is %d meters' % cs.alt
if cs.alt > 10: 	
    cs.alt = cs.alt - 1
    # Script.ChangeParam(cs.alt, cs.alt - 1) #set altitude to current altitude - 1 meter
    Script.Sleep(1000)
	# print 'Current Altitude is %d meters' % cs.alt
else:
    print 'Altitude too low'
# print 'done'
	