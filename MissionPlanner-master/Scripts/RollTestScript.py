# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

print 'Starting roll script'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'Current roll is %d degrees' % cs.roll
print 'Beginning roll right'
Script.ChangeParam(cs.roll, cs.roll + 20) #set roll to roll + 20 degrees
Script.Sleep(1000)
Script.ChangeParam(cs.roll, cs.roll - 20) #set roll back to original value
print 'End roll right'

print 'Beginning roll left'
Script.ChangeParam(cs.roll, cs.roll - 20) #set roll to roll - 20 degrees
Script.Sleep(1000)
Script.ChangeParam(cs.roll, cs.roll + 20) #set roll back to original value
print 'End roll left'

Script.ChangeMode("LOITER")
print 'End of roll script'