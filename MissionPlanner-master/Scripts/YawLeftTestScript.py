# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

print 'Starting yaw script'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'Current yaw is %d degrees' % cs.yaw
#Script.ChangeParam(cs.yaw, cs.yaw - 90) #set altitude to current altitude + 1 meter
print 'yaw left'
cs.yaw=cs.yaw -90
Script.Sleep(1000)
print 'Current yaw is now %d degrees' %cs.yaw
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'End of yaw script'