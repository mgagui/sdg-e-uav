# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed #in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

print 'Starting roll script'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'Current roll is %d degrees' % cs.roll
print 'Beginning roll right'

cs.roll = cs.roll + 20;

Script.Sleep(1000)

print 'Current roll is %d degrees' % cs.roll
cs.roll = cs.roll - 20;

print 'End roll right'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'

