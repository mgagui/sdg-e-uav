# 
# RTL + AUTO LAND test by JLN jan 15, 2012
# RTL mode set on CH5
#
print 'Start Script:Take-Off, far away + RTL and Land' 
print 'gogogogogogogogogogogogogogogog'
#Script.SendRC(1,1500,True)  # ch1: roll neutral
#Script.SendRC(2,1500,True)  # ch2: pitch neutral
#Script.SendRC(3,1000,True)  # ch3: throttle neutral
#Script.SendRC(4,1500,True)  # ch4: yaw neutral
#Script.SendRC(5,2000,True)  # ch5: mode Stabilize - Mode 6


#Script.SendRC(4,2000,True) # yaw right to arm the motors
#cs.messages.Clear()
#Script.WaitFor('ARMING MOTORS',60)
#print 'Motors Armed!'
#Script.SendRC(4,1500,True) # ch4: yaw neutral

#Script.Sleep(1000)

print 'pitch forward TEST 1'

Script.SendRC(3,1500,True) # ch3: throttle middle
Script.SendRC(2,800,True) # pitch forward

Script.Sleep(400)

Script.SendRC(1,1500,True)  # ch1: roll neutral
Script.SendRC(2,1500,True)  # ch2: pitch neutral
Script.SendRC(3,1000,True)  # ch3: throttle neutral
Script.SendRC(4,1500,True)  # ch4: yaw neutral

Script.Sleep(400)
Script.ChangeMode('LOITER')

print 'End of RTL test'

#x=1000;
#mavlink_manual_control_t;
