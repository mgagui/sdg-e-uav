# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

print 'Starting pitch script'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'Current pitch is %d degrees' % cs.pitch
print 'Beginning pitch forward'
Script.ChangeParam(cs.pitch, cs.pitch + 20) #set pitch to pitch + 20 degrees
Script.Sleep(1000)
Script.ChangeParam(cs.pitch, cs.pitch - 20) #set pitch back to original value
print 'End pitch forward'

print 'Beginning pitch backwards'
Script.ChangeParam(cs.pitch, cs.pitch - 20) #set pitch to pitch - 20 degrees
Script.Sleep(1000)
Script.ChangeParam(cs.pitch, cs.pitch + 20) #set pitch back to original value
print 'End pitch backwards'

print 'End of pitch script'