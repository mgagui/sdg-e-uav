# Script = options are 
# Script.Sleep(ms)
# Script.ChangeParam(name,value)
# Script.GetParam(name)
# Script.ChangeMode(mode) - same as displayed in mode setup screen 'AUTO'
# Script.WaitFor(string,timeout)
# Script.SendRC(channel,pwm,sendnow)
# RC channel 3 - Throttle

print 'Starting ascend script'
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'Current Altitude is %d meters' % cs.alt
#Script.ChangeParam(cs.alt, cs.alt + 1) #set altitude to current altitude + 1 meter
cs.alt= cs.alt + 1
Script.Sleep(1000)
print 'Current Altitude is now %d meters' %cs.alt
Script.ChangeMode("LOITER")
print 'Now in Loiter mode'
print 'End of ascend script'