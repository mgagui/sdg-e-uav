# 
# RTL + AUTO LAND test by JLN jan 15, 2012
# RTL mode set on CH5
# (int channel, ushort pwm, bool sendnow)
print 'Start Script:Take-Off, far away + RTL and Land' 
Script.SendRC(1,1500,True)  # ch1: roll neutral
Script.SendRC(2,1500,True)  # ch2: pitch neutral
Script.SendRC(3,1000,True)  # ch3: throttle neutral
Script.SendRC(4,1500,True)  # ch4: yaw neutral
Script.SendRC(5,2000,True)  # ch5: mode Stabilize - Mode 6

Script.Sleep(200)

while cs.lat == 0:
	print 'Waiting for GPS'
	Script.Sleep(1000)
print 'Got GPS'

Script.Sleep(200)

Script.SendRC(4,2000,True) # yaw right to arm the motors
cs.messages.Clear()
Script.WaitFor('ARMING MOTORS',600)
print 'Motors Armed!'
Script.SendRC(4,1500,True) # ch4: yaw neutral

Script.Sleep(1000)

print 'Takeoff'
Script.SendRC(1,1500,True) # ch1: roll neutral
Script.SendRC(2,1500,True) # ch2: pitch neutral
Script.SendRC(3,1600,True) # ch3: throttle up
Script.SendRC(4,1500,True) # ch4: yaw neutral

while cs.alt < 10:
	Script.Sleep(50)
	

