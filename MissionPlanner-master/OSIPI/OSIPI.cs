﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PISDK;
using PISDKCommon;
using PITimeServer;
using System.Data.OleDb;

namespace OSIPI {
    public class OSIPIsystem {

        PISDK.Server piServer;                                      //Create instance of PI Server                                    

        //<<<<<<<<<<Connect to PI Server>>>>>>>>>>
        public void connectToServer(string piServerName)
        {
            PISDK.PISDK SDK = new PISDK.PISDK();                    // Create new instance of PI SDK
            piServer = SDK.Servers[piServerName];                   // Assign PI Server to local machine
            piServer.Open(piServer.DefaultUser);                    // Open connection through default user
        }

        //<<<<<<<<<<Check if connection established>>>>>>>>>>
        public Boolean isConnectedToServer()
        {
            if (piServer != null) return piServer.Connected;        // Return true if connected
            else return false;                                      // Return false is not connected
        }

        //<<<<<<<<<< Connect if not already connected >>>>>>>>>>
        public void connectIfNotConnected(string piServerName)
        {
            if (piServer == null)
            {
                connectToServer("esmartserver-pc");
            }
        }

        //<<<<<<<<<<Disconnect from the server>>>>>>>>>>
        public void dissconnectFromServer()
        {
            try
            {
                piServer.Close();                                       // Close the connection
            }
            catch
            {
            }
        }

        // set pi point value 
        public void setPiPointValue(string tagName, object value)
        {
            PISDK.PIValue piValue = new PISDK.PIValue();            // Create new instance of PIValue
            piValue.Value = value;                                  // Set the Pi Value

            PITimeServer.PITime piTime = new PITimeServer.PITime(); // Create new instance of PITime
            piValue.TimeStamp = piTime;                             //Set the datetime   

            //Add entry to server
            piServer.PIPoints[tagName].Data.UpdateValue(piValue, "*", PISDK.DataMergeConstants.dmInsertDuplicates, null);
        }

        // Create new PI Point
        public void createPiPoint(String tagName, String pointClass, PointTypeConstants pointTypeConstant , string description)
        {
            // check if it already exists
            if (piServer.GetPoints(String.Format("tag='{0}'", tagName)).Count == 1){

            }// otherwise add to the PI Server
            else{
                var newAttribs = new PISDKCommon.NamedValues();
                newAttribs.Add("Descriptor", description);
                newAttribs.Add("compressing", 0);                       // no compression

                piServer.PIPoints.Add(tagName, pointClass, pointTypeConstant, newAttribs);
            }
            //example: ("MyNewDigitalPoint", "classic", PointTypeConstants.pttypInt16, null)
        }

        // Create a bunch of telemetry points per mission
        public void createTelemetryPoints(String tagName){

            createPiPoint(tagName + "lat", "classic", PISDK.PointTypeConstants.pttypFloat32, "Latitude.");                                  // latitude value
            createPiPoint(tagName + "lon", "classic", PISDK.PointTypeConstants.pttypFloat32, "Longitude");                                  // longatude value
            createPiPoint(tagName + "alt", "classic", PISDK.PointTypeConstants.pttypFloat16, "Altitude.");                                  // altitude (meters)  
            createPiPoint(tagName + "bat", "classic", PISDK.PointTypeConstants.pttypFloat16, "Battery remaining.");                         // bat remaining
            createPiPoint(tagName + "heading", "classic", PISDK.PointTypeConstants.pttypInt16, "Heading in degrees.");                      // heading
            createPiPoint(tagName + "groundSpeed", "classic", PISDK.PointTypeConstants.pttypFloat16, "Ground speed m/s.");                  // latitude values    
            createPiPoint(tagName + "current", "classic", PISDK.PointTypeConstants.pttypFloat16, "Current running through the line.");      // latitude values    
        }

        //<<<<<<<<<<Retrieve current value from existing PI Point>>>>>>>>>>
        public object getCurrentValue(String tagName)
        {
            //Retrive the latest value given the tage name
            PIValues val = piServer.PIPoints[tagName].Data.RecordedValuesByCount("*", 1, DirectionConstants.dReverse);
            return val[1].Value;
        }

        //<<<<<<<<<<Retrieve all the values from existing PI Point>>>>>>>>>>
        //NOTE: does not work
        public List<string> getValues(String tagName, DateTime startTime, DateTime endTime, TimeSpan span)
        {
            List<string> Svalues = new List<string>();
            List<PIValue> values = new List<PIValue>();         // Create List instance
            PIPoint point = piServer.PIPoints[tagName];         // Create point instance
            DateTime tempTime = startTime;                      // Set a temp start time for while loop

            while (tempTime < endTime)                          // Loop until endtime is reached
            {
                //Add data to values and increase the tempTime by the timespan
                values.Add(point.Data.ArcValue(tempTime, RetrievalTypeConstants.rtAtOrBefore));
                tempTime += span;
            }

            //Transfer to String list
            foreach (PIValue value in values)
            {
                if (value.ValueAttributes.GetType().IsCOMObject)
                {
                    Svalues.Add((value.Value as DigitalState).Name.ToString());
                }
                else
                {
                    Svalues.Add((value.Value).ToString());
                }
            }
            return Svalues;
        }

        //<<<<<<<<<<Remove an existing Pi Point>>>>>>>>>>
        public void deletePiPoint(string tagName)
        {
            piServer.PIPoints.Remove(tagName);
        }

       
    }
}